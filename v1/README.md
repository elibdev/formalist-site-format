
# Formalist Site Format 1.0.0-pre

## Purpose of This Document

This document is intended to serve as a an authoritative specification of the external format of a Formalist site. This specification does not include anything about the internal structure or implementation of a site. Clearly documenting the format helps us communicate design decisions and also provides an opportunity for anyone to make alternative, compatible implementations.

## What is Formalist?

Formalist is a tool for taking control of your digital content. The goal of Formalist is to bring to the usability of social media together with an independent web presence. The format of a Formalist site resembles a social media account in terms of its tag-based organizational system and mobile-centric design, but it is designed to be fully functional as a website. 

There are some important ways that social media is simpler than, and more accessible than today's website builders. They also represent a shift towards a more centralized model of authority and control. Social media comes with all of the limits of the platform, including arbitrary policy enforcement (or lack thereof), digital identity tracking, and the dynamics of heavily weighted "social metrics." 

Formalist borrows its basic concepts from social media:

- everything is a post
- simple tag system for organizing posts
- clean minimal interface for presenting posts

A Formalist site is just a collection of posts organized by tag, presented in a specific format.

## Definitions

### Building Blocks

__site__: a published website displaying posts and tags. 

__post__: the basic unit of publishing. A post has content text, which includes its headline and tags. 

__tag__: a named series of related posts. 

__headline__: a portion of text that is used to represent a post. The headline text is the post's first line, or its first 300 characters.

__cover post__: a post displayed at the top of the home page. 

### Reader Interface

__post page__: a page displaying a post's title, description, content, and links to the previous and next article for each of the post's tags. 

__tag page__: a page displaying all posts with the given tag. The name of the tag is used as the title of the page. The title and a brief preview of the content of each post is displayed below. 

__home page__: a page displaying links to all tag pages with the description of the tag, followed by title links to the first four posts of each tag. 

### Editor Interface

__tag list__: an interface for viewing all the tags within a site in order of most frequently used.

__post list__: an interface for viewing all posts of a given tag in order of most recently created.

__post editor__: an interface for editing an individual post.

### API

__site API__: an HTTPS interface to all of Formalist's raw functionality that enables extensions and integrations with other applications. 

__message__: the JSON body of a POST request to or 200 reply from the site API. 

__action__: a specific type of message that you can send to the site API to perform an action or get some data. The top-level JSON should be an object with an `action` property that contains a valid action name. The rest of the object should be formatted properly for that action. 

### Authentication

__token__: a random string of characters that must be included in all API requests that require authentication. You get a new token when you sign in to your account. Multiple tokens can be valid simultaneously. When you reset your passphrase, all existing tokens are revoked. 

__reset link / setup link__: a URL that includes a random string of characters that serves as a pre-authenticated one-time link to initiate a passphrase reset, or an initial passphrase setup. 

__passphrase__: a sequence of 5 automatically generated space-separated dictionary words that is used to securely sign in to an account. 

__passphrase hint__: a sequence of words that serves as a mnemonic hint for a given passphrase. The passphrase is not related to the passphrase hint in any way, except to serve as a mnemonic link for the user.

## General Site Structure

Websites in Formalist all have the same general structure, determined entirely by their tags. 

Every page on the site is one of:

- home page (`/`)
- post page (`/post/:post-id`)
- tag page (`/tag/:tag-name`)

The home page consists of an optional home post at the top of the page, followed by a list of all the site's tags with links to a few posts of each tag. 

If a post has multiple tags, it will be displayed on multiple tag pages (once for each tag it has). 

## Automatic Links

You can create a link to another webpage by including a valid URL in the content of the post. 

Every valid URL gets converted to a link element that includes the target page's title, description, and domain. The link element is placed where the URL was written. 

There is intentionally no way to provide your own text for the link. Using the metadata of the URL to create the link ensures that links clearly represent where they lead. 

## Using Tags

Tags are the primary means of organizing posts on a Formalist site. 

You can tag a post by including the tag in the content of the post. A tag is represented as a `#` followed by any number of characters (except `.`, `,`, `:`, or `;`) with no spaces. Capitalization is ignored, so "#EatGood" and "#eatgood" are the same tag. 

If you include tags by themselves at the end of a post, they are not displayed as part of the post's content, but are still applied to the post. 

## Creating a Home Page

A site's home page consists of the account name, a cover post, and a list of all tags used on the site (ordered by post count) with headline links to the most recent three posts in each tag. 

## Site API Overview

Every site has its own HTTPS API that enables extensions and integrations with other applications. The site API has an unconventional structure, but it is simple and consistent. 

Every valid request to the API is one of several actions that belong to one of three general categories:

1. authentication: `signIn`, `signOut`, `resetPassphrase`
2. getting site data: `getSite`
3. updating site data: `editName`, `editEmail`, `createPost`, `editPost`, `editCover`

The API has only one endpoint that you send HTTPS requests to and receive replies from. Every proper request is a `POST` to the same endpoint with a JSON body, returning a JSON reply with a `200` status code. The only thing that changes between different requests is the content of the body and the reply. When we say "message," we are referring to only the body's JSON content or reply. 

Each message sent to the API should have a valid action in its `action` property, and the rest of the message should be valid for that action. 

## API Message Format

For convenience and readability, we use a method of abbreviating HTTP requests in this document where a message exchange written like this:

```json
->
{
  "action": "signIn",
  "email": "name@example.com",
  "passphrase": "how orange yam stick face"
}

<-
{"token": "fv0EMe0olbYCWvFQ"}
```

refers to an HTTP request-reply exchange like this:

```http
->
POST https://www.example.com/api/v1
Content-Type: application/json

{
  "action": "signIn",
  "email": "name@example.com",
  "passphrase": "how orange yam stick face"
}

<-
200 OK
Content-Type: application/json

{"token": "fv0EMe0olbYCWvFQ"}
```

## API Error Handling

There are three main categories of errors you can encounter using the API:

1. __improper API usage:__ returns `400` status code
2. __message received but action not understood:__ returns `200` status code with `undefined` as body; may be implemented later
3. __action understood and caused error:__ returns `200` status code with body containing error info

### Email-based Passphrase Setup and Reset

When a new account is created, the user is emailed a link to set up a new passphrase. The passphrase setup process is identical to the passphrase reset process, and the steps are as follows:

- user creates new account or initiates password reset
- an email is sent to the user's email with a one-time link
- user visits email link to view the new passphrase only once (passphrase is reset only here, and all tokens are revoked)
- user logs in using the new passphrase

The request made by clicking the passphrase reset link looks like this:

```http
->
GET https://www.example.com/api/v1/?reset=ah73jdrh9f72b7_1h

<-
200 OK
Content-Type: application/json

{
  "passphrase": "how red yam stick face",
  "passphraseHint": "total begin"
}
```

### Authenticating with the API

The first step to using the API for any purpose is to authenticate by providing the passphrase. If authentication succeeds, the reply will include a token that you can use to access the full API functionality. 

Every action except `signIn` and `resetPassphrase` requires a valid token to be included in the message object. 

- __`signIn`__

__Purpose:__ Provide an email and passphrase to authenticate with a site to get a token that gives you controlled access to the full API functionality. 

__Returns:__ `{"token": "accessToken"}` on successful authentication with a token to be used in subsequent messages, or `"error"` on failed authentication. 

__Example:__

```json
->
{
  "action": "signIn", 
  "email": "name@example.com",
  "passphrase": "how orange yam stick face"
}

<-
{"token": "fv0EMe0olbYCWvFQ"}
```

- __`signOut`__

__Purpose:__ Sign out to manually revoke a token. 

__Returns:__ `"ok"`

__Example:__

```json
-> 
{
  "action": "signOut", 
  "token": "fv0EMe0olbYCWvFQ"
}

<- "ok"
```

- __`resetPassphrase`__

__Purpose:__ Send a passphrase reset link to the registered email. 

__Returns:__ "ok"

__Example:__

```json
-> 
{"action": "resetPassphrase"}

<- 
"ok"
```

### Getting Site Data with the API

There is a single action `getSite` that retrieves all the site's data. 

- __`getSite`__

__Purpose:__ Retrieve all site data. 

__Returns:__ All the site's data as a single JSON object

__Example:__

```json
-> 
{
  "action": "getSite", 
  "token": "fv0EMe0olbYCWvFQ"
}

<- 
{
  "name": "accountName",
  "email": "name@example.com",
  "posts": [ {
    "id": 1,
    "content": "I'm #shy.\nI don't say much."
  }, {
    "id": 2,
    "content": "Eat peanut butter for #gains."
  } ]
}
```

### Updating Site Data with the API

- __`editName`__

__Purpose:__ Edit the name of the account. The name cannot contain any spaces. 

__Returns:__ `"ok"` if the new name is valid, or `"error"` if it's invalid. 

__Example:__

```json
-> 
{
  "action": "editName", 
  "token": "fv0EMe0olbYCWvFQ", 
  "name": "new_name"
}

<- 
"ok"
```

-  __`editEmail`__

__Purpose:__ Edit the email used for the account. 

__Returns:__ `"ok"` if the new email is a valid email address, or `"error"` if it's not.

__Example:__

```json
-> 
{
  "action": "editEmail", 
  "token": "fv0EMe0olbYCWvFQ", 
  "email": "new_email@example.com"
}

<- 
"ok"
```

- __`createPost`__

__Purpose:__ Create a new post with the given content.

__Returns:__ An object with an `id` property containing the ID of the newly created post. 

__Example:__

```json
-> 
{
  "action": "createPost", 
  "token": "fv0EMe0olbYCWvFQ", 
  "content": "I'm #shy."
}

<- 
{"id": 1}
```

- __`editPost`__

__Purpose:__ Edit an existing post with the given ID so that it has the given content. 

__Returns:__ `"ok"` if the post exists, or `"error"` if it doesn't.

__Example:__

```json
-> 
{
  "action": "editPost", 
  "token": "fv0EMe0olbYCWvFQ", 
  "id": 1, 
  "content": "I'm #shy.\nI don't say much."
}

<- 
"ok"
```

- __`editCover`__

__Purpose:__ Edit the cover post so that it's content is the given text. 

__Returns:__ "ok"

__Example:__

```json
-> 
{
  "action": "editCover", 
  "token": "fv0EMe0olbYCWvFQ", 
  "content": "some stray thoughts"
}

<- 
"ok"
```
