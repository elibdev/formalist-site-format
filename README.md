# Formalist Site Format

This document is intended to serve as a an authoritative specification of the external format of a Formalist site.

Read the current version of the format: 

### [Formalisit Site Format 1.0.0-pre](/v1/README.md) 

Changes to this document are [proposed and planned on Trello.](https://trello.com/b/yCxgmruK) 

The [Formalist classic style](https://github.com/sunsetworks/formalist-classic-style) is documented separately. 
